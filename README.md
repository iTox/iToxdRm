
<!-- README.md is generated from README.Rmd. Please edit that file -->

# iToxdRm R-package

This is a collection of R functions which can be handy in the daily life
of iTox.

To install the package go to R, and install the package devtools first:

``` r
install.packages('devtools')
```

Afterwards, install the iToxdRm package by typing:

``` r
devtools::install_git('https://git.ufz.de/itox/iToxdRm.git')
```

# How to use the package

The package contains a couple of example datasets.

``` r
library(iToxdRm)
library(tidyverse)

count_data = iToxdRm::count_data
head(count_data)
```

<div class="kable-table">

| substance | exposure_end_hpf | concentration | n_testorganism | effect_lethal_count | effect_sublethal_count | unit   |
|:----------|-----------------:|--------------:|---------------:|--------------------:|-----------------------:|:-------|
| naproxen  |               48 |    1354.86864 |              9 |                   3 |                      9 | umol_l |
| naproxen  |               48 |     677.43432 |              9 |                   0 |                      7 | umol_l |
| naproxen  |               48 |     338.71716 |              9 |                   0 |                      0 | umol_l |
| naproxen  |               48 |     169.35858 |              9 |                   0 |                      0 | umol_l |
| naproxen  |               48 |      84.67929 |              9 |                   0 |                      0 | umol_l |
| naproxen  |               48 |      42.33964 |              9 |                   0 |                      0 | umol_l |

</div>

``` r

continuous_data = iToxdRm::continuous_data
head(continuous_data)
```

<div class="kable-table">

| experiment_id | timepoint_hpf | effect_value | concentration | unit   | substance |
|--------------:|--------------:|-------------:|--------------:|:-------|:----------|
|            31 |            48 |          150 |      11.56102 | umol/L | mix3      |
|            31 |            48 |          156 |      17.34153 | umol/L | mix3      |
|            31 |            48 |          153 |      26.01229 | umol/L | mix3      |
|            31 |            48 |          150 |      26.01229 | umol/L | mix3      |
|            31 |            48 |          162 |      26.01229 | umol/L | mix3      |
|            31 |            48 |          156 |      39.01844 | umol/L | mix3      |

</div>

``` r

sce_data = iToxdRm::sce_data
head(sce_data)
```

<div class="kable-table">

|      logFC | concentration_umol_l | concentration_level | time_hpe | ensembl_gene_id    | nodeID |
|-----------:|---------------------:|:--------------------|---------:|:-------------------|-------:|
|  0.0499738 |                 0.00 | Control             |       48 | ENSDARG00000091085 |   1062 |
| -0.0499738 |                 0.00 | Control             |       48 | ENSDARG00000091085 |   1062 |
|  0.6324686 |                 0.00 | Control             |       72 | ENSDARG00000091085 |   1062 |
| -0.3433949 |                 0.00 | Control             |       72 | ENSDARG00000091085 |   1062 |
| -0.2890737 |                 0.00 | Control             |       72 | ENSDARG00000091085 |   1062 |
| -0.9222948 |                 9.45 | C1                  |       48 | ENSDARG00000091085 |   1062 |

</div>

To generate usable count data from Intob `effects_summary.csv` needs to
be further summarised:

``` r
intob_data = read_csv('...')
# grouping by condition and summarising counts for lethal and sublethal effects
intob_data = intob_data %>% group_by(concentration, age_embryo_observation_hpf, test_substance, experiment_id) %>% 
  summarise(
    n_testorganism = n(),
    effect_lethal_count = sum(lethal),
    effect_sublethal_count = sum(sublethal),
    total_count = sum(lethal) + sum(sublethal),
    .groups = 'drop'
  ) %>% 
  # calculate relative values of lethal and sublethal counts
  # this is not needed in iToxdRm but is useful for modelling with other methods (e.g. drc)
  mutate(
    lethal = .$effect_lethal_count / .$n_testorganism,
    sublethal = .$effect_sublethal_count / .$n_testorganism,
    total = .$total_count / .$n_testorganism
  )
```

## Fitting count data

### fit_drm_count()

This function fits a dose response model for count data (binomial
distribution)

``` r
drm_count = fit_drm_count(
    concentration = count_data %>% filter(exposure_end_hpf == 72) %>% .$concentration,
    effect_count = count_data %>% filter(exposure_end_hpf == 72) %>% .$effect_lethal_count,
    n_testorganism = count_data %>% filter(exposure_end_hpf == 72) %>% .$n_testorganism,
    plot = T
)
#> Warning: Transformation introduced infinite values in continuous x-axis
```

<img src="man/figures/README-unnamed-chunk-6-1.png" style="display: block; margin: auto;" />

Values like the LC50 can be found in the ECx_frame of the resulting
object.

``` r
sprintf('The LC50 is %.2f', drm_count$ECx_frame$concentration[drm_count$ECx_frame$response == 0.5])
#> [1] "The LC50 is 596.37"
```

### fit_count_batch()

This function does dose response modelling for count data in batch. It
calls `fit_drm_count()` individually on the data subset for each
combination of substance and timepoint.

``` r
drm_count_batch = fit_count_batch(
    concentration = count_data$concentration,
    effect_count = count_data$effect_lethal_count,
    n_testorganism = count_data$n_testorganism,
    substance = count_data$substance,
    time = count_data$exposure_end_hpf,
    plot = F
)

drm_count_batch$data_measured %>% 
    ggplot(aes(x = concentration, y = response)) +
    geom_point(aes(colour = factor(time))) +
    geom_path(
        data = drm_count_batch$ECx_frame,
        mapping = aes(x = concentration, y = response, colour = factor(time))
    ) +
    scale_x_log10() +
    theme_classic() +
    theme(legend.position = 'bottom')
#> Warning: Transformation introduced infinite values in continuous x-axis
```

<img src="man/figures/README-unnamed-chunk-8-1.png" style="display: block; margin: auto;" />

## Fitting continuous data

### fit_drm_continuous()

``` r
drm_cont = fit_drm_continuous(
    concentration = continuous_data %>% filter(substance == 'mix3' & timepoint_hpf == 48) %>% .$concentration,
    response = continuous_data %>% filter(substance == 'mix3' & timepoint_hpf == 48) %>% .$effect_value
)
#> maxr was not supplied by user and was therefore set to max(response) = 162.00
```

<img src="man/figures/README-unnamed-chunk-9-1.png" style="display: block; margin: auto;" />

### fit_continuous_batch()

``` r
drm_cont_batch = fit_continuous_batch(
    concentration = continuous_data$concentration,
    response = continuous_data$effect_value,
    substance = continuous_data$substance,
    time = continuous_data$timepoint_hpf,
    plot = F
)
#> maxr was not supplied by user and was therefore set to max(response) = 201.00

drm_cont_batch$data_measured %>% 
    ggplot(aes(x = concentration, y = response)) +
    geom_point(aes(colour = factor(time))) +
    geom_path(
        data = drm_cont_batch$ECx_frame,
        mapping = aes(x = concentration, y = response, colour = factor(time))
    ) +
    scale_x_log10() +
    theme_classic() +
    theme(legend.position = 'bottom')
```

<img src="man/figures/README-unnamed-chunk-10-1.png" style="display: block; margin: auto;" />

## Dose response modelling with shuffled complex evolution optimisation

### set_param_bounds()

First, parameter bounds are computed from the concentrations

``` r
param_bounds = set_param_bounds(sce_data$concentration_umol_l)
param_bounds
#> $slope
#>         min         max 
#>  0.01329977 18.33268694 
#> 
#> $X50
#>          min          max 
#>    0.4668924 3871.3452804 
#> 
#> $mconc
#>          min          max 
#>    0.4668924 3871.3452804 
#> 
#> $sconc
#>        min        max 
#>  0.1651821 21.2141232 
#> 
#> $err
#>   min   max 
#> 1e-02 1e+03
```

### fit_drm_sce()

``` r
response_df = tibble(
    response = sce_data %>% filter(time_hpe == 72) %>% .$logFC,
    dose = sce_data %>% filter(time_hpe == 72) %>% .$concentration_umol_l
)

sce_params = fit_drm_sce(
    dose = response_df$dose,
    response = response_df$response,
    param_bounds = param_bounds
)
sce_params
#>      hillslope_up_hill            X50_up_hill            err_up_hill 
#>           4.473544e+00           6.596937e+01           8.951264e-01 
#>    hillslope_down_hill          X50_down_hill          err_down_hill 
#>           1.395332e+01           1.078257e+03           2.845818e+00 
#>    hillslope_best_hill          X50_best_hill          err_best_hill 
#>           4.473544e+00           6.596937e+01           8.951264e-01 
#>          max_best_hill         mconc_up_gauss         sconc_up_gauss 
#>           5.419086e+00           1.238231e+02           5.239185e-01 
#>           err_up_gauss       mconc_down_gauss       sconc_down_gauss 
#>           6.469732e-01           3.702088e+03           2.618744e-01 
#>         err_down_gauss       mconc_best_gauss       sconc_best_gauss 
#>           2.845828e+00           1.238231e+02           5.239185e-01 
#>         err_best_gauss         max_best_gauss             AIC_spline 
#>           6.469732e-01           5.419086e+00           4.309448e+01 
#>               AIC_null            AIC_up_hill          AIC_down_hill 
#>           1.167229e+02           7.289380e+01           1.284153e+02 
#>           AIC_up_gauss         AIC_down_gauss           AICw_up_hill 
#>           5.731352e+01           1.284153e+02           1.000000e+00 
#>          AICw_up_gauss         AICw_down_hill        AICw_down_gauss 
#>           1.000000e+00           2.882576e-03           2.882576e-03 
#>      AICw_vspline_hill     AICw_vspline_gauss    Convergence_up_hill 
#>           3.381887e-07           8.166190e-04           0.000000e+00 
#>  Convergence_down_hill   Convergence_up_gauss Convergence_down_gauss 
#>           0.000000e+00           0.000000e+00           0.000000e+00 
#>                maxr_up              maxr_down                 startr 
#>           5.419086e+00          -1.351749e+00           0.000000e+00 
#>         direction_hill        direction_gauss             best_model 
#>           1.000000e+00           1.000000e+00           2.000000e+00 
#>         best_model_dir                    log 
#>           1.000000e+00           1.000000e+00
```

The model can be plotted with `plot_hill()`. Besides the hill model
itself, it contains confidence intervals for the model and for the
controls.

``` r
plot_sce(
    parameters = sce_params,
    dose = response_df$dose,
    response = response_df$response,
    title = ''
)
#> Warning: Transformation introduced infinite values in continuous x-axis
```

<img src="man/figures/README-unnamed-chunk-13-1.png" style="display: block; margin: auto;" />

### fit_sce_batch()

This functions runs `fit_drm_sce()` in batch for each id (two timepoints
in this case). It returns a data frame with one row per identifier.

``` r
sce_params_batch = fit_sce_batch(
    ids = sce_data$time_hpe,
    dose = sce_data$concentration_umol_l,
    response = sce_data$logFC,
    param_bounds = param_bounds
)
sce_params_batch
```

<div class="kable-table">

| hillslope_up_hill | X50_up_hill | err_up_hill | hillslope_down_hill | X50_down_hill | err_down_hill | hillslope_best_hill | X50_best_hill | err_best_hill | max_best_hill | mconc_up_gauss | sconc_up_gauss | err_up_gauss | mconc_down_gauss | sconc_down_gauss | err_down_gauss | mconc_best_gauss | sconc_best_gauss | err_best_gauss | max_best_gauss | AIC_spline |  AIC_null | AIC_up_hill | AIC_down_hill | AIC_up_gauss | AIC_down_gauss | AICw_up_hill | AICw_up_gauss | AICw_down_hill | AICw_down_gauss | AICw_vspline_hill | AICw_vspline_gauss | Convergence_up_hill | Convergence_down_hill | Convergence_up_gauss | Convergence_down_gauss |  maxr_up |  maxr_down | startr | direction_hill | direction_gauss | best_model | best_model_dir | log |  ID |
|------------------:|------------:|------------:|--------------------:|--------------:|--------------:|--------------------:|--------------:|--------------:|--------------:|---------------:|---------------:|-------------:|-----------------:|-----------------:|---------------:|-----------------:|-----------------:|---------------:|---------------:|-----------:|----------:|------------:|--------------:|-------------:|---------------:|-------------:|--------------:|---------------:|----------------:|------------------:|-------------------:|--------------------:|----------------------:|---------------------:|-----------------------:|---------:|-----------:|-------:|---------------:|----------------:|-----------:|---------------:|----:|----:|
|          1.866739 |    35.55768 |   0.5080204 |            10.79729 |      2352.138 |      1.662130 |            1.866739 |      35.55768 |     0.5080204 |      2.760200 |        95.2080 |      0.8886541 |    0.4904843 |         1822.798 |        0.1732288 |       1.662119 |          95.2080 |        0.8886541 |      0.4904843 |       2.760200 |   41.34363 |  75.28785 |    42.98501 |      95.14236 |     41.43829 |       95.14236 |    0.9999999 |             1 |      0.0000488 |       0.0000488 |         0.3056177 |          0.4881706 |                   0 |                     0 |                    0 |                      0 | 2.760200 | -0.9222948 |      0 |              1 |               1 |          2 |              1 |   1 |  48 |
|          4.473544 |    65.96937 |   0.8951264 |            13.95332 |      1078.257 |      2.845818 |            4.473544 |      65.96937 |     0.8951264 |      5.419086 |       123.8231 |      0.5239185 |    0.6469732 |         3702.088 |        0.2618744 |       2.845828 |         123.8231 |        0.5239185 |      0.6469732 |       5.419086 |   43.09448 | 116.72295 |    72.89380 |     128.41532 |     57.31352 |      128.41532 |    1.0000000 |             1 |      0.0028826 |       0.0028826 |         0.0000003 |          0.0008166 |                   0 |                     0 |                    0 |                      0 | 5.419086 | -1.3517490 |      0 |              1 |               1 |          2 |              1 |   1 |  72 |

</div>

``` r
plotlist = lapply(c(48, 72), function(t) {
    plot_sce(
        parameters = sce_params_batch[sce_params_batch$ID == t,],
        dose = sce_data %>% filter(time_hpe == t) %>% .$concentration_umol_l,
        response = sce_data %>% filter(time_hpe == t) %>% .$logFC,
        title = sprintf('t = %ihpe', t)
    )
})
cowplot::plot_grid(
    plotlist = plotlist
)
#> Warning: Transformation introduced infinite values in continuous x-axis
#> Transformation introduced infinite values in continuous x-axis
```

<img src="man/figures/README-unnamed-chunk-15-1.png" style="display: block; margin: auto;" />

## Other functions

### get_CI_diffs()

This functions calculates confidence interval (CI) differences. These
are the differences between the confidence interval of the model and the
confidence interval of the controls (lower boundary of model and upper
boundary of controls for a model in the positive direction or vice versa
for a model in the negative direction) for each concentration. The
`diff_hill` parameter in the resulting data frame describes the
differences. For example, in the model from the example data above at 72
hpe, there are CI values for the three largest concentrations, but not
the two lower concentrations as the CIs intersect here.

``` r
get_CI_diffs(
    parameters = sce_params,
    response_df = response_df
)
```

<div class="kable-table">

|            |   dose | logFC_hill | logFC_gauss |   n |   se_hill | max_hill |   min_hill |  se_gauss | max_gauss |  min_gauss | diff_hill | diff_gauss |
|:-----------|-------:|-----------:|------------:|----:|----------:|---------:|-----------:|----------:|----------:|-----------:|----------:|-----------:|
| 1          |   9.45 |   0.000909 |   0.0000314 |  18 | 0.8951264 | 1.755324 | -1.7535064 | 0.6469732 |  1.268076 | -1.2680128 | 0.0000000 |  0.0000000 |
| 2          |  25.75 |   0.079393 |   0.0606626 |  18 | 0.8951264 | 1.833808 | -1.6750224 | 0.6469732 |  1.328707 | -1.2073816 | 0.0000000 |  0.0000000 |
| 3          |  70.18 |   3.082155 |   3.0122337 |  18 | 0.8951264 | 4.836571 |  1.3277398 | 0.6469732 |  4.280278 |  1.7441895 | 0.1676974 |  0.5841471 |
| 4          | 115.86 |   5.015337 |   5.3756449 |  18 | 0.8951264 | 6.769753 |  3.2609218 | 0.6469732 |  6.643689 |  4.1076007 | 2.1008794 |  2.9475583 |
| 5          | 191.27 |   5.373156 |   3.8401433 |  18 | 0.8951264 | 7.127572 |  3.6187408 | 0.6469732 |  5.108187 |  2.5720990 | 2.4586984 |  1.4120567 |
| ControlCIs |   0.00 |   0.000000 |   0.0000000 |   6 |        NA | 1.160042 | -0.9477902 |        NA |  1.160042 | -0.9477902 |        NA |         NA |

</div>

### get_CI_diffs_batch()

And the same function for batches. This returns a list of data frames,
one for each identifier.

``` r
ci_diffs_batch = get_CI_diffs_batch(
    ids = sce_data$time_hpe,
    dose = sce_data$concentration_umol_l,
    response = sce_data$logFC,
    paramframe = sce_params_batch
)
summary(ci_diffs_batch)
#>      Length Class      Mode
#> [1,] 13     data.frame list
#> [2,] 13     data.frame list
```

### get_sig_level()

``` r
get_sig_level()
```

### clip_data()

``` r
clip_data()
```
