#' get CI differences
#'
#' @param parameters a data frame containing fitted parameter values obtained with fit_drm_sce()
#' @param response_df data frame with columns 'dose' and 'response'
#'
#' @return a data frame with ci diffs
#' @export
#'
get_CI_diffs <-
  function(parameters,
             response_df) {

    # define regression function -----------------------------------------------------------------------
    abund.funct <- function(dose, hillslope, X50, maxr) {
      maxr / (1 + exp(-hillslope * (log(dose) - log(X50))))
    }

    ## Gauss-Gaus ---------------------------
    abund.funct_gaugau <- function(dose, mconc, sconc, maxr) {
      maxr * exp(-((((log(dose) - log(mconc))^2) / (2 * (sconc^2)))))
    }



    treatmentns <-
      nrow(response_df[response_df$dose != 0, ])

    controls <-
      response_df[response_df$dose == 0, ]

    controlns <- nrow(response_df[response_df$dose == 0, ])


    # take quantiles --------------------------------------------------------
    ControlCIs <- c(quantile(controls$response, c(0.025, 0.975)))

    names(ControlCIs) <-
      c("min_hill", "max_hill")
    ControlCIs["n"] <- controlns
    ControlCIs["max_gauss"] <- ControlCIs["max_hill"]
    ControlCIs["min_gauss"] <- ControlCIs["min_hill"]

    # calculate confidence interval for treatment ---------------------------
    modeldf <- data.frame(dose = unique(response_df$dose[response_df$dose != 0]))


    modeldf$logFC_hill <- NA
    modeldf$logFC_gauss <- NA
    modeldf$n <- treatmentns

    # hill ------------------------------------------------------------
    modeldf$logFC_hill <-
      abund.funct(
        dose = modeldf$dose,
        hillslope = as.numeric(unlist(parameters["hillslope_best_hill"])),
        X50 = as.numeric(unlist(parameters["X50_best_hill"])),
        maxr = as.numeric(unlist(parameters["max_best_hill"]))
      )

    modeldf$se_hill <-
      as.numeric(unlist(parameters["err_best_hill"]))
    modeldf$max_hill <-
      modeldf$logFC_hill + (qnorm(0.975) * modeldf$se_hill)
    modeldf$min_hill <-
      modeldf$logFC_hill - (qnorm(0.975) * modeldf$se_hill)



    # gauss-gauss ------------------------------------------------------------
    modeldf$logFC_gauss <-
      abund.funct_gaugau(
        dose = modeldf$dose,
        mconc = as.numeric(unlist(parameters["mconc_best_gauss"])),
        sconc = as.numeric(unlist(parameters["sconc_best_gauss"])),
        maxr = as.numeric(unlist(parameters["max_best_gauss"]))
      )

    modeldf$se_gauss <-
      as.numeric(unlist(parameters["err_best_gauss"]))
    modeldf$max_gauss <-
      modeldf$logFC_gauss + (qnorm(0.975) * modeldf$se_gauss)
    modeldf$min_gauss <-
      modeldf$logFC_gauss - (qnorm(0.975) * modeldf$se_gauss)


    # calculate difference for each measured treatment -----------------------

    # hill-gauss -----
    modeldf$diff_hill <-
      apply(
        modeldf,
        MARGIN = 1,
        FUN = function(treatment) {
          if (as.numeric(treatment["min_hill"]) > 0) {
            dif <-
              as.numeric(treatment["min_hill"]) - ControlCIs["max_hill"]

            if (length(dif) == 0) {
              dif <- 0
            } # if controls were removed as outliers
            dif[dif < 0] <- 0
            return(dif)
          } else {
            if (as.numeric(treatment["max_hill"]) < 0) {
              dif <-
                as.numeric(treatment["max_hill"]) - ControlCIs["min_hill"]
              if (length(dif) == 0) {
                dif <- 0
              } # if controls were removed as outliers
              dif[dif > 0] <- 0
              return(dif)
            } else {
              return(0)
            }
          }
        }
      )

    # gauss-gauss ------
    modeldf$diff_gauss <-
      apply(
        modeldf,
        MARGIN = 1,
        FUN = function(treatment) {
          if (as.numeric(treatment["min_gauss"]) > 0) {
            dif <-
              as.numeric(treatment["min_gauss"]) - ControlCIs["max_gauss"]
            if (length(dif) == 0) {
              dif <- 0
            } # if controls were removed as outliers
            dif[dif < 0] <- 0
            return(dif)
          } else {
            if (as.numeric(treatment["max_gauss"]) < 0) {
              dif <-
                as.numeric(treatment["max_gauss"]) - ControlCIs["min_gauss"]
              if (length(dif) == 0) {
                dif <- 0
              } # if controls were removed as outliers
              dif[dif > 0] <- 0
              return(dif)
            } else {
              return(0)
            }
          }
        }
      )

    # add controls -----
    ControlCIs["dose"] <- 0
    ControlCIs["logFC_hill"] <- 0
    ControlCIs["diff_hill"] <- NA
    ControlCIs["se_hill"] <- NA
    ControlCIs["logFC_gauss"] <- 0
    ControlCIs["diff_gauss"] <- NA
    ControlCIs["se_gauss"] <- NA

    modeldf <- rbind(modeldf, t(data.frame(ControlCIs)))

    return(modeldf)
  }

